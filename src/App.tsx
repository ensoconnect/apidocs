import * as React from 'react';
import logo from './logo.svg';
import './App.css';

import { RedocStandalone } from 'redoc';

function App() {
  return (
    <div className="App">
        <RedocStandalone specUrl="https://enso-specs.s3.amazonaws.com/apis/enso_console.json" />
    </div>
  );
}

export default App;
